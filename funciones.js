

function animarUltimoParrafo() {
    document.location.href = '#ancla';
    document.getElementById('ancla').classList.add('animate__animated', 'animate__jackInTheBox');
    document.getElementById('ancla').classList.add('animate-duration', '2s');
}


// Esta función se ejecuta al hacer click en "iniciar sesión" en la página de login.html
function iniciarSesion() {
    var nombreUsuario = document.getElementById('nombreUsuario').value;
    var pass = document.getElementById('password').value;

    if (nombreUsuario.toLowerCase() == 'jazmin' && pass == '1234') {
        document.getElementById('notificacion').innerText = 'Bienvenida!!';
        localStorage.setItem("usuario", nombreUsuario);
        window.location.href = "index.html";
    } else {
        document.getElementById('notificacion').innerText = 'Usuario o contraseña incorrecta';
    }
}

// Esta función se ejecuta cuando termina de cargar el <body> de index.html
function verificarInicioDeSesion() {
    document.readyState
    var nombreUsuario = localStorage.getItem('usuario');

    if (nombreUsuario == null) {
        // No hay sesión abierta entonces muestro el botón de iniciar sesión
        document.getElementById('linkIniciarSesion').innerHTML = '<a class="color" href="login.html">Iniciar sesión</a>';
        return;
    } else {
        // La sesión está iniciada. Entonces muestro el botón de cerrar sesión
        document.getElementById('saludoBienvenida').innerText = 'Hola ' + nombreUsuario + '!!!';
        document.getElementById('linkCerrarSesion').innerHTML = '<a class="color" href="javascript:void(0)" onclick="cerrarSesion()">Cerrar sesión</a>';
    }
}

// Este es un nuevo comentario
function cerrarSesion() {
    localStorage.removeItem('usuario');
    window.location.href = "index.html";
    console.log('La sesión fue cerrada');
    console.log('Sesion cerrada OK.')
}